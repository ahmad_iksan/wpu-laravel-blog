@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">
                <article class="mb-3">
                    <h1 class="mb-3">{{ $post->title }}</h1>

                    <p>By. <a href="/posts?author={{ $post->author->username }}"
                            class="text-decoration-none">{{ $post->author->name }}</a> in
                        <a href="/posts?category={{ $post->category->slug }}"
                            class="text-decoration-none">{{ $post->category->name }}</a>
                    </p>

                    @if ($post->image)
                        <div style="max-height: 350px; overflow:hidden">
                            <img src="{{ asset('storage/' . $post->image) }}" class="img-fluid" alt="">
                        </div>
                    @else
                        <img src="https://source.unsplash.com/1200x600/?{{ $post->category->name }}" class="img-fluid mb-3"
                            alt="">
                    @endif

                    {!! $post->body !!}

                </article>


                <a href="/posts">Back to Post</a>
            </div>
        </div>
    </div>
@endsection
