@extends('layouts.main')

@section('content')

    <h2 class="mb-3 text-center">{{ $title }}</h2>

    <div class="row mb-3">
        <div class="col-md-6  mx-auto">
            <form action="/posts">
                @if (request('category'))
                    <input type="hidden" name="category" value="{{ request('category') }}">
                @endif
                @if (request('author'))
                    <input type="hidden" name="author" value="{{ request('author') }}">
                @endif
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="search" value="{{ request('search') }}"
                        placeholder="Search..">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @if ($posts->count())
        <div class="card mb-3">

            <div style="max-height: 400px; overflow:hidden;">
                @if ($posts[0]->image)
                    <img src="{{ asset('storage/' . $posts[0]->image) }}" class="img-fluid" alt="">
                @else
                    <img src="https://source.unsplash.com/1200x600/?{{ $posts[0]->category->name }}" class="img-fluid"
                        alt="">
                @endif
            </div>
            <div class="card-body text-center">
                <h3 class="card-title"><a href="/post/{{ $posts[0]->slug }}"
                        class="text-decoration-none text-dark">{{ $posts[0]->title }}</a></h3>
                <p>
                    <small class="text-muted">
                        By. <a href="/posts?author={{ $posts[0]->author->username }}"
                            class="text-decoration-none">{{ $posts[0]->author->name }}</a>
                        In <a href="/posts?category={{ $posts[0]->category->slug }}"
                            class="text-decoration-none">{{ $posts[0]->category->name }}</a>
                        {{ $posts[0]->created_at->diffForHumans() }}
                    </small>
                </p>
                <p class="card-text">{{ $posts[0]->excerpt }}</p>
                <a href="/post/{{ $posts[0]->slug }}" class="btn btn-primary">Read more</a>
            </div>
        </div>

        <div class="container">
            <div class="row">
                @foreach ($posts->skip(1) as $post)
                    <div class="col-md-4 mb-3">
                        <div class="card">
                            <div style="position: absolute;background-color: rgba(0,0,0,0.7);padding: 5px 15px;">
                                <a class="text-decoration-none text-light"
                                    href="/posts?category={{ $post->category->slug }}">{{ $post->category->name }}</a>
                            </div>
                            @if ($post->image)
                                <img src="{{ asset('storage/' . $post->image) }}" class="img-fluid" alt="">
                            @else
                                <img src="https://source.unsplash.com/1200x600/?{{ $post->category->name }}"
                                    class="img-fluid" alt="">
                            @endif
                            <div class="card-body">
                                <h4><a href="/post/{{ $post->slug }}"
                                        class="text-decoration-none">{{ $post->title }}</a>
                                </h4>
                                <p>
                                    <small class="text-muted">
                                        By. <a href="/posts?author={{ $post->author->username }}"
                                            class="text-decoration-none">{{ $post->author->name }}</a>
                                        {{ $post->created_at->diffForHumans() }}
                                    </small>
                                </p>
                                <p class="card-text">{{ $post->excerpt }}</p>
                                <a href="/post/{{ $post->slug }}" class="btn btn-primary">Read more</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>


    @else
        <p class="text-center fs-3">No post found</p>
    @endif

    <div class="d-flex justify-content-center">
        {{ $posts->links() }}
    </div>


@endsection
