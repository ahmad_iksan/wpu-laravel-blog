@extends('dashboard.layouts.main')

@section('content')

    <div class="container">
        <div class="row justify-content-start my-3">
            <div class="col-lg-8">
                <article class="mb-3">
                    <h1 class="mb-3">{{ $post->title }}</h1>

                    <a href="/dashboard/posts" class="btn btn-sm btn-success"><span data-feather="arrow-left"></span> Back to
                        all
                        my posts</a>
                    <a href="/dashboard/posts" class="btn btn-sm btn-warning"><span data-feather="edit"></span> Edit</a>
                    <a href="/dashboard/posts" class="btn btn-sm btn-danger"><span data-feather="x-circle"></span>
                        Delete</a>
                    @if ($post->image)
                        <div style="max-height: 350px; overflow:hidden">
                            <img src="{{ asset('storage/' . $post->image) }}" class="img-fluid my-3" alt="">
                        </div>
                    @else
                        <img src="https://source.unsplash.com/1200x600/?{{ $post->category->name }}" class="img-fluid my-3"
                            alt="">
                    @endif

                    {!! $post->body !!}

                </article>
            </div>
        </div>
    </div>
@endsection
