<?php

use App\Http\Controllers\AdminCategoryController;
use App\Http\Controllers\DashboadPostController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegisterController;
use App\Models\Category;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* 
    route model binding 
    {post:slug} 
    ->post nama variabel parameter
    ->:slug nama kolom yang akan dicocokan dengan nama variabel parameter(post).
    nama variabel parameter pada function closure / controller harus sama dengan-
    nama variabel parametIsAdminer pada route model binding
*/

Route::get('/', function () {
    return view('home', [
        "title" => "Home"
    ]);
});

Route::get('/about', function () {
    return view('about', [
        "title" => "About"
    ]);
});

Route::get('/posts', [PostController::class, 'index']);

/* route model binding */
Route::get('post/{post:slug}', [PostController::class, 'post']);

Route::get('/categories', function () {
    return view('categories', [
        "title" => 'Post Categories',
        "categories" => Category::all()
    ]);
});

Route::middleware(['guest'])->group(function () {
    Route::get('/login', [LoginController::class, 'index'])->name('login');
    Route::post('/login', [LoginController::class, 'authenticate']);
    Route::get('/register', [RegisterController::class, 'index']);
    Route::post('/register', [RegisterController::class, 'store']);
});

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', DashboardController::class);
    Route::post('/logout', LogoutController::class);
    Route::get('/dashboard/posts/createSlug', [DashboadPostController::class, 'createSlug']);
    Route::resource('/dashboard/posts', DashboadPostController::class);
});


Route::resource('/dashboard/categories', AdminCategoryController::class)->except('show')->middleware('is_admin');


/* route model binding */
// Route::get('/categories/{category:slug}', function (Category $category) {
//     return view('posts', [
//         "title" => "Post By Category : $category->name",
//         "posts" => $category->posts->load('author', 'category')
//     ]);
// });

/* route model binding */
/* 
    nama variabel parameter pada route 
    harus sama dengan
    nama variabel parameter pada closure / controller
*/
// Route::get('/authors/{author:username}', function (User $author) {
//     return view('posts', [
//         'title' => "Post By Author : $author->name ",
//         'posts' => $author->posts->load('author', 'category')
//     ]);
// });
